package com.twuc.webApp;

public class ContractWithSetter {
    String name;
    int id;

    ContractWithSetter() {};

    ContractWithSetter(String name, int id) {
        this.name = name;
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public int getId() {
        return id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setId(int id) {
        this.id = id;
    }
}
