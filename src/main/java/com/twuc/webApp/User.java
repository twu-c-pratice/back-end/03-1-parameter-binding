package com.twuc.webApp;

import javax.validation.constraints.*;

public class User {
    @NotNull
    @Size(min = 2, max = 9)
    private String name;

    @Max(9999)
    @Min(1000)
    private int yearOfBirth;

    @Email private String email;

    public User(String name, @Max(9999) @Min(1000) int yearOfBirth, @Email String email) {
        this.name = name;
        this.yearOfBirth = yearOfBirth;
        this.email = email;
    }

    public User() {}

    public String getName() {
        return name;
    }

    public int getYearOfBirth() {
        return yearOfBirth;
    }

    public String getEmail() {
        return email;
    }
}
