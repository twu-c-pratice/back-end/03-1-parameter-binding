package com.twuc.webApp;

import java.time.ZonedDateTime;

public class DateTimeContainer {
    private ZonedDateTime dateTime;

    public DateTimeContainer() {}

    public DateTimeContainer(ZonedDateTime dateTime) {
        this.dateTime = dateTime;
    }

    public ZonedDateTime getDateTime() {
        return dateTime;
    }
}
