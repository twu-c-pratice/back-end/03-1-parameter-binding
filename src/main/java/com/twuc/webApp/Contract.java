package com.twuc.webApp;

public class Contract {
    String name;
    int id;

    Contract() {};

    Contract(String name, int id) {
        this.name = name;
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public int getId() {
        return id;
    }
}
