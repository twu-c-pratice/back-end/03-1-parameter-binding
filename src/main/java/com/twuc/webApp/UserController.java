package com.twuc.webApp;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
public class UserController {
    @GetMapping("/api/users/{userId}")
    String getBooksByUserId(@PathVariable Integer userId) {
        return String.format("books of user %d", userId);
    }

    @GetMapping("/api/users/primitive/{userId}")
    String getBooksByUserIdPrimitive(@PathVariable int userId) {
        return String.format("books of user %d primitive", userId);
    }

    @GetMapping("/api/users/{userId}/books/{bookId}")
    String getBookByUserIdAndBookId(@PathVariable int userId, @PathVariable int bookId) {
        return String.format("%d of %d", userId, bookId);
    }

    @GetMapping("/api/books")
    String getBookByUserIdAndBookIdQuery(@RequestParam int userId, @RequestParam int bookId) {
        return String.format("%d of %d", userId, bookId);
    }

    @GetMapping("/api/books/default")
    String getBookByUserIdAndBookIdQueryWithDefault(
            @RequestParam(defaultValue = "2") int userId,
            @RequestParam(defaultValue = "3") int bookId) {
        return String.format("%d of %d", userId, bookId);
    }

    @GetMapping("/api/users/collection")
    String getMultiplyUsersByIds(@RequestParam List<Integer> id) {
        return "ID are " + id;
    }

    @PostMapping("/api/datetime")
    DateTimeContainer getDatetime(@RequestBody DateTimeContainer dateTime) {
        return dateTime;
    }

    @PostMapping("/api/datetime/defined")
    DateTimeContainer getDateTimeDefined(
            @RequestBody @DateTimeFormat(pattern = "yyyy-MM-dd") DateTimeContainer dateTime) {
        return dateTime;
    }

    @PostMapping("/api/not-null")
    ResponseEntity validNotNull(@RequestBody @Valid User user) {
        return ResponseEntity.ok().body(user.getName());
    }

    @PostMapping("/api/null")
    ResponseEntity validNull(@RequestBody @Valid User user) {
        return ResponseEntity.ok().build();
    }

    @PostMapping("/api/too-long")
    ResponseEntity tooLong(@RequestBody @Valid User user) {
        return ResponseEntity.ok().build();
    }

    @PostMapping("/api/year-of-birth")
    ResponseEntity outOfRange(@RequestBody @Valid User user) {
        return ResponseEntity.ok().build();
    }

    @PostMapping("/api/email")
    ResponseEntity emailFormatError(@RequestBody @Valid User user) {
        return ResponseEntity.ok().build();
    }
}
