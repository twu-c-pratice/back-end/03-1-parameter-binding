package com.twuc.webApp;

public class ContractWithoutDefaultConstroctor {
    String name;
    int id;

    ContractWithoutDefaultConstroctor(String name, int id) {
        this.name = name;
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public int getId() {
        return id;
    }
}
