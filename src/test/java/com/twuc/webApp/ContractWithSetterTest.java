package com.twuc.webApp;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;

import java.io.IOException;

import static org.junit.jupiter.api.Assertions.assertEquals;

class ContractWithSetterTest {
    @Test
    public void should_return_contract_string() throws JsonProcessingException {
        String expectName = "hi";
        int expectId = 119;
        ContractWithSetter contract = new ContractWithSetter(expectName, expectId);
        ObjectMapper objectMapper = new ObjectMapper();
        String contractString = objectMapper.writeValueAsString(contract);
        assertEquals("{\"name\":\"hi\",\"id\":119}", contractString);
    }

    @Test
    public void should_get_contract_object() throws IOException {
        String expectName = "hi";
        int expectId = 119;
        ContractWithSetter expectContract = new ContractWithSetter(expectName, expectId);
        ObjectMapper objectMapper = new ObjectMapper();
        String contractString = "{\"name\":\"hi\",\"id\":119}";
        ContractWithSetter contract =
                objectMapper.readValue(contractString, ContractWithSetter.class);
        assertEquals(expectContract.getId(), contract.getId());
        assertEquals(expectContract.getName(), contract.getName());
    }
}
