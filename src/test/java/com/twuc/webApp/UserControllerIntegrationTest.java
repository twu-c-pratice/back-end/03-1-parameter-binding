package com.twuc.webApp;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
class UserControllerIntegrationTest {
    @Autowired MockMvc mockMvc;

    @Test
    public void should_return_books_by_user_id() throws Exception {
        int userId = 2;
        String expect = String.format("books of user %s", userId);
        mockMvc.perform(get("/api/users/" + String.valueOf(userId)))
                .andExpect(content().string(expect));
    }

    @Test
    public void should_return_books_by_user_id_primitive() throws Exception {
        int userId = 2;
        String expect = String.format("books of user %s primitive", userId);
        mockMvc.perform(get("/api/users/primitive/" + String.valueOf(userId)))
                .andExpect(content().string(expect));
    }

    @Test
    public void should_return_book_by_user_id_and_book_id() throws Exception {
        int userId = 2;
        int bookId = 3;
        String except = String.format("%s of %s", bookId, userId);
        mockMvc.perform(get(String.format("/api/users/%d/books/%d", bookId, userId)))
                .andExpect(content().string(except));
    }

    @Test
    public void should_return_book_by_user_id_and_book_id_query() throws Exception {
        int userId = 2;
        int bookId = 3;
        String except = String.format("%s of %s", bookId, userId);
        mockMvc.perform(get(String.format("/api/books?userId=%s&bookId=%s", userId, bookId)))
                .andExpect(content().string(except));
    }

    @Test
    public void should_failed_when_without_param() throws Exception {
        int userId = 2;
        int bookId = 3;
        String except = String.format("%s of %s", bookId, userId);
        mockMvc.perform(get(String.format("/api/books", userId, bookId)))
                .andExpect(status().is4xxClientError());
    }

    @Test
    public void should_return_default_when_without_param() throws Exception {
        int userId = 2;
        int bookId = 3;
        String except = String.format("%s of %s", userId, bookId);
        mockMvc.perform(get(String.format("/api/books/default", userId, bookId)))
                .andExpect(content().string(except));
    }

    @Test
    public void should_return_when_with_collection() throws Exception {
        String excepted = "ID are [1, 2, 3]";
        mockMvc.perform(get("/api/users/collection?id=1,2,3"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().string(excepted));
    }

    @Test
    void should_return_datatime_string() throws Exception {
        String dateTimeJson = "{\"dateTime\":\"2019-10-01T10:00:00Z\"}";
        mockMvc.perform(
                        post("/api/datetime")
                                .contentType(MediaType.APPLICATION_JSON_UTF8)
                                .content(dateTimeJson))
                .andExpect(content().string(dateTimeJson));
    }

    @Test
    public void should_return_datatime_string_defined() throws Exception {
        String dateTimeJson = "{\"dateTime\":\"2019-10-01\"}";
        mockMvc.perform(
                        post("/api/datetime")
                                .contentType(MediaType.APPLICATION_JSON_UTF8)
                                .content(dateTimeJson))
                .andExpect(content().string(dateTimeJson));
    }

    @Test
    void should_get_error_if_null() throws Exception {
        mockMvc.perform(
                        post("/api/null")
                                .contentType(MediaType.APPLICATION_JSON_UTF8)
                                .content("{\"name\":null}"))
                .andExpect(status().is(400));
    }

    @Test
    void should_get_ok_if_not_null() throws Exception {
        mockMvc.perform(
                        post("/api/not-null")
                                .contentType(MediaType.APPLICATION_JSON_UTF8)
                                .content("{\"name\":\"not-null\"}"))
                .andExpect(status().is(200))
                .andExpect(content().string("not-null"));
    }

    @Test
    void should_get_error_if_too_long() throws Exception {
        mockMvc.perform(
                        post("/api/too-long")
                                .contentType(MediaType.APPLICATION_JSON_UTF8)
                                .content(
                                        "{\"name\":\"1234567890\",\"yearOfBirth\":8888,\"email\":\"abc@gmail.com\"}"))
                .andExpect(status().is(400));
    }

    @Test
    void should_get_error_if_year_of_birth_is_out_of_range() throws Exception {
        mockMvc.perform(
                        post("/api/year-of-birth")
                                .contentType(MediaType.APPLICATION_JSON_UTF8)
                                .content(
                                        "{\"name\":\"123456\",\"yearOfBirth\":10000,\"email\":\"abc@gmail.com\"}"))
                .andExpect(status().is(400));
    }

    @Test
    void should_get_error_if_email_is_not_ok() throws Exception {
        mockMvc.perform(
                        post("/api/email")
                                .contentType(MediaType.APPLICATION_JSON_UTF8)
                                .content(
                                        "{\"name\":\"123456\",\"yearOfBirth\":8888,\"email\":\"abc\"}"))
                .andExpect(status().is(400));
    }
}
